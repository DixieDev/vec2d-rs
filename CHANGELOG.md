# 3.1.0

- Removed the proc macro, clearing the dependency list
- Improved handling conversions with some `as_` functions
- Added `Hash` to list of derived traits
- Added `min_each`, `max_each`, and `clamp_each` functions where elements impl `PartialOrd`

# 3.0.0

Thanks to @nyovaya for all of the following contributions:

- Updated to Rust 2021 edition
- Added `Index` impl
- Added `Default` impl
- `fn new` is now `const`
- Conversions now impl `From` instead of `Into`
