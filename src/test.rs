use crate::Vector2D;

#[test]
fn dot() {
    let v1 = Vector2D::new(10.0, 5.0);
    let v2 = Vector2D::new(1.5, 2.0);

    let result = Vector2D::dot(v1, v2);

    assert_eq!(25.0, result);
}

#[test]
fn from_vec2d() {
    let iv = Vector2D::new(10, 5);
    let fv = Vector2D::from_vec2d(iv);
    assert_eq!(Vector2D::new(10.0, 5.0), fv);
}

#[test]
fn into_vec2d() {
    let iv = Vector2D::new(10, 5);
    let fv = iv.into_vec2d();
    assert_eq!(Vector2D::new(10.0, 5.0), fv);
}

#[test]
fn from_tuple() {
    let ituple = (10, 5);
    let fv = ituple.into();
    assert_eq!(Vector2D::new(10.0, 5.0), fv);
}

#[test]
fn from_array() {
    let arr = [10, 5];
    let fv = arr.into();
    assert_eq!(Vector2D::new(10.0, 5.0), fv);
}

#[test]
fn length_squared() {
    let v = Vector2D::new(10, 5);
    let r = v.length_squared();
    assert_eq!(125, r);
}

#[test]
fn length_f32() {
    let v: Vector2D<f32> = Vector2D::new(3.0, 4.0);
    let r: f32 = v.length();
    assert_eq!(5.0, r);
}

#[test]
fn length_f64() {
    let v: Vector2D<f64> = Vector2D::new(3.0, 4.0);
    let r: f64 = v.length();
    assert_eq!(5.0, r);
}

#[test]
fn angle_f32() {
    let v: Vector2D<f32> = Vector2D::new(2.0, 2.0);
    let r: f32 = v.angle();
    assert_eq!(std::f32::consts::PI / 4.0, r);
}

#[test]
fn angle_f64() {
    let v: Vector2D<f64> = Vector2D::new(2.0, 2.0);
    let r: f64 = v.angle();
    assert_eq!(std::f64::consts::PI / 4.0, r);
}

#[test]
fn add() {
    let v1 = Vector2D::new(10.0, 5.0);
    let v2 = Vector2D::new(1.5, 2.0);

    let result = v1 + v2;

    assert_eq!(Vector2D::new(11.5, 7.0), result);
}

#[test]
fn add_assign() {
    let mut v1 = Vector2D::new(10.0, 5.0);
    let v2 = Vector2D::new(1.5, 2.0);

    v1 += v2;

    assert_eq!(Vector2D::new(11.5, 7.0), v1);
}
#[test]
fn sub() {
    let v1 = Vector2D::new(10.0, 5.0);
    let v2 = Vector2D::new(1.5, 2.0);

    let result = v1 - v2;

    assert_eq!(Vector2D::new(8.5, 3.0), result);
}

#[test]
fn sub_assign() {
    let mut v1 = Vector2D::new(10.0, 5.0);
    let v2 = Vector2D::new(1.5, 2.0);

    v1 -= v2;

    assert_eq!(Vector2D::new(8.5, 3.0), v1);
}

#[test]
fn mul() {
    let v = Vector2D::new(10.0, 5.0);
    let f = 2.0;

    let result = v * f;

    assert_eq!(Vector2D::new(20.0, 10.0), result);
}

#[test]
fn mul_assign() {
    let mut v = Vector2D::new(10.0, 5.0);
    let f = 2.0;

    v *= f;

    assert_eq!(Vector2D::new(20.0, 10.0), v);
}

#[test]
fn div() {
    let v = Vector2D::new(10.0, 5.0);
    let f = 2.0;

    let result = v / f;

    assert_eq!(Vector2D::new(5.0, 2.5), result);
}

#[test]
fn div_assign() {
    let mut v = Vector2D::new(10.0, 5.0);
    let f = 2.0;

    v /= f;

    assert_eq!(Vector2D::new(5.0, 2.5), v);
}

#[test]
fn f64_as_i32() {
    let fv: Vector2D<f64> = Vector2D::new(10.5, 11.2);
    let iv = fv.as_i32s();
    assert_eq!(Vector2D::new(10, 11), iv);
}

#[test]
fn f32_as_u32() {
    let fv: Vector2D<f32> = Vector2D::new(10.5, 11.2);
    let uv = fv.as_u32s();
    assert_eq!(Vector2D::new(10, 11), uv);
}

#[test]
fn f32_as_u32_bounded() {
    let fv: Vector2D<f32> = Vector2D::new(-10.5, -11.2);
    let uv = fv.as_u32s();
    assert_eq!(Vector2D::new(0, 0), uv);
}

#[test]
fn lerp() {
    let start = Vector2D::new(5.0, 10.0);
    let end = Vector2D::new(10.0, 11.5);

    let result = Vector2D::lerp(start, end, 0.5);

    assert_eq!(Vector2D::new(7.5, 10.75), result);
}

#[test]
fn neg() {
    let v = Vector2D::new(10.3, -5.4);
    assert_eq!(Vector2D::new(-10.3, 5.4), -v);
}

#[test]
fn default() {
    assert_eq!(
        Vector2D::new(f32::default(), f32::default()),
        Vector2D::default()
    );
}

#[test]
fn index() {
    let v: Vector2D<f32> = Vector2D::new(12.4, 0.5);
    assert_eq!(v[0], v.x);
    assert_eq!(v[1], v.y);
    assert_ne!(v[0], v[1]);
    assert_ne!(v[0], v.y);
    assert_ne!(v[1], v.x);
}

#[test]
fn conversion_bounds_i32() {
    let vec_i32 = Vector2D::new(-100i32, -100i32);
    let vec_u32 = vec_i32.as_u32s();
    let vec_u64 = vec_i32.as_u64s();
    assert_eq!(vec_u32, (0u32, 0u32).into());
    assert_eq!(vec_u64, (0u64, 0u64).into());
}

#[test]
fn conversion_bounds_i64() {
    let big_vec_i64 = Vector2D::new(i64::MAX, i64::MAX);
    let vec_i32 = big_vec_i64.as_i32s();
    let vec_u32 = big_vec_i64.as_u32s();
    assert_eq!(vec_i32, (i32::MAX, i32::MAX).into());
    assert_eq!(vec_u32, (u32::MAX, u32::MAX).into());

    let neg_vec_i64 = Vector2D::new(-100i64, -100i64);
    let vec_u32 = neg_vec_i64.as_u32s();
    let vec_u64 = neg_vec_i64.as_u64s();
    assert_eq!(vec_u32, (0u32, 0u32).into());
    assert_eq!(vec_u64, (0u64, 0u64).into());
}

#[test]
fn conversion_bounds_u32() {
    let vec_u32 = Vector2D::new(u32::MAX, u32::MAX);
    let vec_i32 = vec_u32.as_i32s();
    assert_eq!(vec_i32, (i32::MAX, i32::MAX).into());
}

#[test]
fn conversion_bounds_u64() {
    let vec_u64 = Vector2D::new(u64::MAX, u64::MAX);
    let vec_i32 = vec_u64.as_i32s();
    let vec_i64 = vec_u64.as_i64s();
    assert_eq!(vec_i32, (i32::MAX, i32::MAX).into());
    assert_eq!(vec_i64, (i64::MAX, i64::MAX).into());
}

#[test]
fn clamp_each() {
    let unclampable = Vector2D::new(80i32, 70i32);
    let clamped_unclampable = unclampable.clamp_each(60, 90);
    assert_eq!(clamped_unclampable, unclampable);

    let clampable = Vector2D::new(100i32, 50i32);
    let clamped_clampable = clampable.clamp_each(60, 90);
    assert_eq!(clamped_clampable, (90, 60).into());
}

#[test]
fn min_each() {
    let big = Vector2D::new(100i32, 200i32);
    let min_big = big.min_each(20i32);
    assert_eq!(min_big, (20, 20).into());

    let small = Vector2D::new(15i32, 5i32);
    let min_small = small.min_each(20i32);
    assert_eq!(min_small, small);
}

#[test]
fn max_each() {
    let big = Vector2D::new(100i32, 200i32);
    let max_big = big.max_each(20i32);
    assert_eq!(max_big, big);

    let small = Vector2D::new(15i32, 5i32);
    let max_small = small.max_each(20i32);
    assert_eq!(max_small, (20, 20).into());
}

#[test]
fn clamp_each_f64() {
    let unclampable = Vector2D::new(80.0f64, 70.0f64);
    let clamped_unclampable = unclampable.clamp_each(60.0, 90.0);
    assert_eq!(clamped_unclampable, unclampable);

    let clampable = Vector2D::new(100.0f64, 50.0f64);
    let clamped_clampable = clampable.clamp_each(60.0, 90.0);
    assert_eq!(clamped_clampable, (90.0, 60.0).into());
}

#[test]
fn min_each_f64() {
    let big = Vector2D::new(100.0f64, 200.0f64);
    let min_big = big.min_each(20.0f64);
    assert_eq!(min_big, (20.0, 20.0).into());

    let small = Vector2D::new(15.0f64, 5.0f64);
    let min_small = small.min_each(20.0f64);
    assert_eq!(min_small, small);
}

#[test]
fn max_each_f64() {
    let big = Vector2D::new(100.0f64, 200.0f64);
    let max_big = big.max_each(20.0f64);
    assert_eq!(max_big, big);

    let small = Vector2D::new(15.0f64, 5.0f64);
    let max_small = small.max_each(20.0f64);
    assert_eq!(max_small, (20.0, 20.0).into());
}
